
package jp.co.kayo.android.localplayer;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 *      This program is free software; you can redistribute it and/or modify it under
 *      the terms of the GNU General Public License as published by the Free Software
 *      Foundation; either version 2 of the License, or (at your option) any later
 *      version.
 *      
 *      This program is distributed in the hope that it will be useful, but WITHOUT
 *      ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *      FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *      details.
 *      
 *      You should have received a copy of the GNU General Public License along with
 *      this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *      Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import java.lang.ref.WeakReference;
import java.util.Hashtable;

import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.fragment.ControlFragment;
import jp.co.kayo.android.localplayer.fragment.EqualizerFragment;
import jp.co.kayo.android.localplayer.menu.PresetLoadMenu;
import jp.co.kayo.android.localplayer.menu.PresetSaveMenu;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.IMediaPlayerService;
import jp.co.kayo.android.localplayer.service.IMediaPlayerServiceCallback;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.StrictHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import jp.co.kayo.android.localplayer.util.ViewCache;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;

import android.view.WindowManager.LayoutParams;

@SuppressLint("NewApi")
@TargetApi(9)
public class EqualizerActivity extends BaseActivity  {
    
    private ViewCache mViewCache;
    private SharedPreferences mPref;
    private Handler mHandler = new MyHandler(this);
    
    private static class MyHandler extends Handler {
        WeakReference<EqualizerActivity> ref;

        MyHandler(EqualizerActivity r) {
            ref = new WeakReference<EqualizerActivity>(r);
        }
        
        @Override
        public void handleMessage(Message msg) {
            EqualizerActivity main = ref.get();
            if (main != null) {
                if (msg.what == SystemConsts.EVT_LOAD_PRESET) {
                    int preset = msg.arg1;
                    Fragment f = main.getSupportFragmentManager().findFragmentByTag(SystemConsts.TAG_EQUALIZER);
                    if(f != null && f instanceof EqualizerFragment){
                        EqualizerFragment eqf = (EqualizerFragment)f;
                        eqf.loadPreset(preset);
                    }
                }
                else if (msg.what == SystemConsts.EVT_SAVE_PRESET) {
                    int preset = msg.arg1;
                    Fragment f = main.getSupportFragmentManager().findFragmentByTag(SystemConsts.TAG_EQUALIZER);
                    if(f != null && f instanceof EqualizerFragment){
                        EqualizerFragment eqf = (EqualizerFragment)f;
                        eqf.savePreset(preset);
                    }
                }
            }
        }
    }
    
    IMediaPlayerServiceCallback.Stub mCallback = new IMediaPlayerServiceCallback.Stub() {
        ControlFragment control;

        ControlFragment getControll() {
            if (control == null) {
                FragmentManager m = getSupportFragmentManager();
                control = (ControlFragment) m
                        .findFragmentByTag(SystemConsts.TAG_CONTROL);
            }
            return control;
        }

        @Override
        public void updateView(final boolean updatelist) throws RemoteException {
            Logger.d("updateView=" + updatelist);
            // もし、コントロール部分が表示されているならリストを更新してあげて
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    // AlbumList,ArtistList,Song等
                    ControlFragment control = getControll();
                    IMediaPlayerService binder = getBinder();
                    if (binder != null) {
                        try {
                            long media_id = binder.getMediaId();
                            mViewCache.setPosition(binder.getPosition(),
                                    media_id, binder.getPrefetchId());
                        } catch (RemoteException e) {
                        }
                    }

                    if (control != null) {
                        control.updateView();
                    }

                    Fragment f = getSupportFragmentManager().findFragmentByTag(SystemConsts.TAG_EQUALIZER);
                    if(f != null && f instanceof EqualizerFragment){
                        EqualizerFragment eqf = (EqualizerFragment)f;
                        eqf.changedMedia();
                    }
                }
            });
        }

        @Override
        public void updateList() throws RemoteException {
            // もし、再生中のフラグメントが表示されているならリストを更新してあげて
        }

        @Override
        public void onBufferingUpdate(int percent) throws RemoteException {
            // バッファリング中のプログレスバーですよぉ
            ControlFragment f = getControll();
            if (f != null) {
                f.onBufferingUpdate(percent);
            }
        }

        @Override
        public void startProgress(long max) throws RemoteException {
        }

        @Override
        public void stopProgress() throws RemoteException {
        }

        @Override
        public void progress(long pos, long max) throws RemoteException {
        }

        @Override
        public void close() throws RemoteException {
            finish();
        }
    };
    
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        StrictHelper.registStrictMode();
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);
        
        mPref = PreferenceManager.getDefaultSharedPreferences(this);

        setContentView(R.layout.main_visual);

        hideProgressBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle(getString(R.string.hello));

        getWindow().setSoftInputMode(
                LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Audio Volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // ViewCache
        mViewCache = (ViewCache) getSupportFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mBinder == null) {
            Intent service = new Intent(this, MediaPlayerService.class);
            bindService(service, mConnection, 0);
            startService(service);
        }
    }
    
    @Override
    public void onPause() {
        if (mBinder != null) {
            try {
                mBinder.unregisterCallback(mCallback);
                mBinder = null;
            } catch (RemoteException e) {
            }
            unbindService(mConnection);
        }
        super.onPause();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.eq_menu_items, menu);
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (menu == null) {
            return super.onPrepareOptionsMenu(menu);
        }
        
        MenuItem mnu_loadpreset = menu.findItem(R.id.mnu_load_preset);
        PresetLoadMenu loadpreset_provider = (PresetLoadMenu) mnu_loadpreset.getActionProvider();
        if (loadpreset_provider != null) {
            loadpreset_provider.setHandler(mHandler);
        }

        MenuItem mnu_savepreset = menu.findItem(R.id.mnu_save_preset);
        PresetSaveMenu savepreset_provider = (PresetSaveMenu) mnu_savepreset.getActionProvider();
        if (savepreset_provider != null) {
            savepreset_provider.setHandler(mHandler);
        }
        
        return super.onPrepareOptionsMenu(menu);
    }
    

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            Fragment f = getSupportFragmentManager().findFragmentByTag(SystemConsts.TAG_EQUALIZER);
            if(f != null && f instanceof EqualizerFragment){
                switch (item.getItemId()) {
                    case android.R.id.home: {
                        finish();
                    }
                        break;
                }
            }
            return super.onOptionsItemSelected(item);
        } finally {
        }
    }
    
    @Override
    public void setActionBarSubTitle(int stat) {
        if ((stat & AppWidgetHelper.FLG_PLAY) > 0) {
            IMediaPlayerService binder = getBinder();
            long media_id;
            try {
                media_id = binder.getMediaId();
                if (media_id > 0) {
                    Hashtable<String, String> tbl1 = ContentsUtils.getMedia(
                            this, new String[] {
                                    AudioMedia.ALBUM,
                                    AudioMedia.ALBUM_KEY, AudioMedia.ARTIST,
                                    AudioMedia.TITLE
                            }, media_id);
                    String title = tbl1.get(AudioMedia.TITLE);
                    String artist = tbl1.get(AudioMedia.ARTIST);
                    getSupportActionBar().setSubtitle(title + " - " + artist);
                } else {
                    int pos = binder.getPosition();
                    String[] values = binder.getMediaD(pos);
                    if (values != null) {
                        String title = values[0];
                        String artist = values[2];
                        getSupportActionBar().setSubtitle(
                                title + " - " + artist);
                    }
                }
            } catch (RemoteException e) {
            }
        } else {
            getSupportActionBar().setSubtitle(getString(R.string.hello));
        }
    }

    @Override
    IMediaPlayerServiceCallback getCallBack() {
        return mCallback;
    }
    
    @Override
    public void onServiceConnected(IMediaPlayerService binder) {
        if (binder != null) {
            Fragment f = getSupportFragmentManager().findFragmentByTag(SystemConsts.TAG_EQUALIZER);
            if(f != null && f instanceof EqualizerFragment){
                EqualizerFragment eqf = (EqualizerFragment)f;
                eqf.onServiceConnected(binder);
            }
        }
    }

    @Override
    ViewCache getViewCache() {
        return mViewCache;
    }

    @Override
    Handler getHandler() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void hideMenu() {
    }}

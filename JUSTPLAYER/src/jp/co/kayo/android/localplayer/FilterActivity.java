package jp.co.kayo.android.localplayer;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 *      This program is free software; you can redistribute it and/or modify it under
 *      the terms of the GNU General Public License as published by the Free Software
 *      Foundation; either version 2 of the License, or (at your option) any later
 *      version.
 *      
 *      This program is distributed in the hope that it will be useful, but WITHOUT
 *      ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *      FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 *      details.
 *      
 *      You should have received a copy of the GNU General Public License along with
 *      this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 *      Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class FilterActivity extends Activity implements OnClickListener,
        OnEditorActionListener {
    public static final String KEY_RESULT_TEXT = "key.result.text";
    public static final String KEY_RESULT_RATING = "key.result.rating";
    EditText editSearch;
    RatingBar ratingBar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_dialog);

        editSearch = (EditText) findViewById(R.id.editSearch);
        ratingBar1 = (RatingBar) findViewById(R.id.ratingBar1);

        Button btn1 = (Button) findViewById(R.id.btnOk);
        Button btn2 = (Button) findViewById(R.id.btnCancel);

        if (ContentsUtils.isSDCard(this)) {
            ratingBar1.setVisibility(View.GONE);
        }

        if (savedInstanceState == null) {
            Intent intent = getIntent();
            String text = intent.getStringExtra(KEY_RESULT_TEXT);
            editSearch.setText(text);
            editSearch.setSelection(text.length());

            if (ratingBar1.getVisibility() != View.GONE) {
                int rating = intent.getIntExtra(KEY_RESULT_RATING, 0);
                ratingBar1.setRating(rating);
            }
        }
        editSearch.setOnEditorActionListener(this);
        if (btn1 != null) {
            btn1.setOnClickListener(this);
        }
        if (btn2 != null) {
            btn2.setOnClickListener(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnOk) {
            search();
        }
        finish();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            search();
            finish();
            return true;
        }
        return false;
    }

    private void search() {
        Intent intent = new Intent();
        intent.putExtra(KEY_RESULT_TEXT, editSearch.getText().toString());
        if (ratingBar1.getVisibility() != View.GONE) {
            intent.putExtra(KEY_RESULT_RATING, (int) ratingBar1.getRating());
        }
        setResult(RESULT_OK, intent);
    }
}

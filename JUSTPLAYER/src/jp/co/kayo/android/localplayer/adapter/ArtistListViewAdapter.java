package jp.co.kayo.android.localplayer.adapter;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.core.ImageObserverImpl;
import jp.co.kayo.android.localplayer.core.IndexCursorAdapter;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.provider.ContentsUtils.AlbumInfo;
import jp.co.kayo.android.localplayer.util.FileUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ImageObserver;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.FavoriteInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class ArtistListViewAdapter extends IndexCursorAdapter {
    LayoutInflater inflator;
    Context context;
    Handler handler = new Handler();
    Hashtable<String, AlbumInfo> tbl1 = new Hashtable<String, AlbumInfo>();
    Hashtable<String, AlbumInfo> tbl2 = new Hashtable<String, AlbumInfo>();
    String format;
    boolean hidealbumart = false;

    int getColArtist(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.ARTIST);
    }

    int getColArtistKey(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.ARTIST_KEY);
    }

    int getColNumAlbum(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.NUMBER_OF_ALBUMS);
    }

    int getColNumTracks(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.NUMBER_OF_TRACKS);
    }

    int getColRating(Cursor cursor) {
        return cursor.getColumnIndex(MediaConsts.AudioArtist.FAVORITE_POINT);
    }

    public ArtistListViewAdapter(Context context, Cursor c, ViewCache cache) {
        super(context, c, true, cache, MediaConsts.AudioArtist.ARTIST);
        this.context = context;
        format = context.getString(R.string.txt_artist_grid);
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        hidealbumart = pref.getBoolean("key.hideart", false);
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        ViewHolder holder = (ViewHolder) view.getTag();
        if (position % 2 == 1) {
            holder.getBackground().setVisibility(View.GONE);
        } else {
            holder.getBackground().setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        long id = cursor.getLong(cursor
                .getColumnIndex(MediaConsts.AudioArtist._ID));
        String artist = cursor.getString(getColArtist(cursor));
        String artistKey = cursor.getString(getColArtistKey(cursor));
        String nalbums = cursor.getString(getColNumAlbum(cursor));
        String ntracks = cursor.getString(getColNumTracks(cursor));
        int col = getColRating(cursor);

        holder.setPotision(cursor.getPosition());
        holder.getText2().setText(artist);
        holder.getText3().setText(String.format(format, nalbums, ntracks));

        if (col != -1) {
            holder.getRating1().setRating(cursor.getInt(col));
        } else {
            FavoriteInfo inf = getViewcache().getFavorite(context, id,
                    TableConsts.FAVORITE_TYPE_ARTIST);
            holder.getRating1().setRating(inf.rating);
        }

        if (holder.getImage1() != null) {
            if (artist != null && !imageloadskip) {
                // まずはArtistのみの検索で設定する
                Integer artistArtKey = Funcs.getAlbumKey(null, artistKey);
                Bitmap bmp = getViewcache().getImage(
                        null,
                        artistKey,
                        null,
                        new ImageObserverImpl(handler, getViewcache(), holder,
                                artistArtKey, cursor.getPosition()));
                if (bmp != null) {
                    holder.getImage1().setImageBitmap(bmp);
                } else {
                    // ない場合はAlbumから取得して、それをArtistに設定する
                    AlbumInfo art = ContentsUtils.getAlbumArt(context, artist,
                            tbl1, tbl2);
                    if (art != null) {
                        Integer key = Funcs.getAlbumKey(art.album, artist);
                        bmp = getViewcache().getImage(
                                art.album,
                                artist,
                                art.album_art,
                                new MyImageObserverImpl(handler,
                                        getViewcache(), holder, artistArtKey,
                                        key, cursor.getPosition()));
                        if (bmp != null) {
                            File destFile = getViewcache().createAlbumArtFile(
                                    artistArtKey);
                            File srcFile = getViewcache().createAlbumArtFile(
                                    key);
                            if (srcFile.exists()) {
                                try {
                                    FileUtils.copyFile(srcFile, destFile);
                                } catch (IOException e) {
                                }
                            }
                            holder.getImage1().setImageBitmap(bmp);
                        } else {
                            holder.getImage1().setImageBitmap(null);// .setImageResource(R.drawable.albums);
                        }
                    } else {
                        holder.getImage1().setImageBitmap(null);// .setImageResource(R.drawable.albums);
                    }
                }
            } else {
                holder.getImage1().setImageBitmap(null);// .setImageResource(R.drawable.albums);
            }
        }

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = getInflator(context).inflate(
                hidealbumart ? R.layout.text_list_row4
                        : R.layout.img_list_row_artist, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.setImage1((ImageView) v.findViewById(R.id.imageArt));
        holder.setText2((TextView) v.findViewById(R.id.text2));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_PRI_COLOR, holder.getText2());
        holder.setText3((TextView) v.findViewById(R.id.text3));
        getViewcache().getColorset().setColor(ColorSet.KEY_DEFAULT_SEC_COLOR, holder.getText3());
        holder.setRating1((RatingBar) v.findViewById(R.id.ratingBar1));
        holder.setBackground(v.findViewById(R.id.background));
        v.setTag(holder);
        return v;
    }

    private class MyImageObserverImpl implements ImageObserver {

        ViewHolder mholder;
        Handler mHandler;
        Integer artistKey;
        Integer mKey;
        int mPotision;

        public MyImageObserverImpl(Handler handler, ViewCache viewcache,
                ViewHolder holder, Integer artistKey, Integer key, int pos) {
            this.mHandler = handler;
            this.mholder = holder;
            this.artistKey = artistKey;
            this.mKey = key;
            this.mPotision = pos;
        }

        @Override
        public void onLoadImage(final Bitmap bmp) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    File destFile = getViewcache()
                            .createAlbumArtFile(artistKey);
                    File srcFile = getViewcache().createAlbumArtFile(mKey);
                    if (srcFile.exists()) {
                        try {
                            FileUtils.copyFile(srcFile, destFile);
                        } catch (IOException e) {
                        }
                    }

                    if (mPotision == mholder.getPotision()) {
                        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
                        anim.setDuration(200);
                        mholder.getImage1().startAnimation(anim);
                        mholder.getImage1().setImageBitmap(bmp);
                        mholder.getImage1().invalidate();
                    }
                }
            });
        }

    }
}

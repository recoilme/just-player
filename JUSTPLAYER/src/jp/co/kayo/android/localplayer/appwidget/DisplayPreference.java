package jp.co.kayo.android.localplayer.appwidget;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.service.MediaPlayerService;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class DisplayPreference extends Activity {

    public static final String KEY_FONTCOLOR = "KEY_FONTCOLOR";
    public static final String KEY_BACKCOLOR = "KEY_BACKCOLOR";
    public static final String KEY_DISPFRAME = "KEY_DISPFRAME";
    public static int default_fontcolor = Color.argb(255, 255, 255, 255);
    public static int default_backcolor = Color.argb(0, 0, 0, 0);

    ShapeDrawable shape;
    Button textBtn;
    SeekBar textBar;
    Button backBtn;
    SeekBar backBar;
    CheckBox checkFrame;
    TextView text1;
    TextView text2;
    View panelWidget;
    View panelDesktop;
    SharedPreferences mPref;
    int textColor = 0;
    int backColor = 0;

    @Override
    protected void onPause() {
        super.onPause();
        Editor editor = mPref.edit();
        int fontcolor = text1.getTextColors().getDefaultColor();
        int backcolor = shape.getPaint().getColor();

        editor.putInt(KEY_FONTCOLOR, fontcolor);
        editor.putInt(KEY_BACKCOLOR, backcolor);
        editor.putBoolean(KEY_DISPFRAME, checkFrame.isChecked());
        editor.commit();

        Intent i = new Intent(this, MediaPlayerService.class);
        i.setAction(AppWidgetHelper.CALL_UPDATEWIDGET);
        startService(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        new ThemeHelper().selectTheme(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.displaypref);

        mPref = PreferenceManager.getDefaultSharedPreferences(this);

        textColor = mPref.getInt(KEY_FONTCOLOR, default_fontcolor);
        backColor = mPref.getInt(KEY_BACKCOLOR, default_backcolor);

        float[] outerR = new float[] { 8, 8, 8, 8, 8, 8, 8, 8 };
        shape = new ShapeDrawable(new RoundRectShape(outerR, null, null));
        shape.getPaint().setStyle(Style.FILL_AND_STROKE);
        shape.getPaint().setColor(backColor);

        panelWidget = (View) findViewById(R.id.panelWidget);
        textBtn = (Button) findViewById(R.id.textColorBtn);
        textBar = (SeekBar) findViewById(R.id.textSeekBar);
        backBtn = (Button) findViewById(R.id.backColorBtn);
        backBar = (SeekBar) findViewById(R.id.backSeekBar);
        text1 = (TextView) findViewById(R.id.text01);
        text2 = (TextView) findViewById(R.id.text02);
        checkFrame = (CheckBox)findViewById(R.id.checkFrame);

        text1.setTextColor(textColor);
        text2.setTextColor(textColor);
        checkFrame.setChecked(mPref.getBoolean(KEY_DISPFRAME, true));

        textBtn.setText(getColorString(textColor));
        textBar.setProgress(Color.alpha(textColor));
        backBtn.setText(getColorString(backColor));
        backBar.setProgress(Color.alpha(backColor));

        panelWidget.setBackgroundDrawable(shape.getCurrent());

        panelDesktop = (View) findViewById(R.id.panelDesktop);
        BitmapDrawable wallpaper = (BitmapDrawable) getWallpaper();
        if (wallpaper != null)
            panelDesktop.setBackgroundDrawable(wallpaper);

        textBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DisplayPreference.this,
                        ColorPickerDialog.class);
                startActivityForResult(intent, 0);

            }
        });
        textBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                Logger.d("onProgressChanged " + seekBar.getProgress());

                int a = seekBar.getProgress();
                int r = Color.red(textColor);
                int g = Color.green(textColor);
                int b = Color.blue(textColor);
                text1.setTextColor(Color.argb(a, r, g, b));
                text2.setTextColor(Color.argb(a, r, g, b));
                panelWidget.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Logger.d("onStartTrackingTouch " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Logger.d("onStopTrackingTouch " + seekBar.getProgress());

            }
        });

        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DisplayPreference.this,
                        ColorPickerDialog.class);
                startActivityForResult(intent, 1);

            }
        });
        backBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                    boolean fromUser) {
                Logger.d("onProgressChanged " + seekBar.getProgress());

                int a = seekBar.getProgress();
                int org = shape.getPaint().getColor();
                shape.getPaint().setStyle(Style.FILL);
                shape.getPaint().setColor(
                        Color.argb(a, Color.red(org), Color.green(org),
                                Color.blue(org)));
                panelWidget.setBackgroundDrawable(shape.getCurrent());
                panelWidget.invalidate();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Logger.d("onStartTrackingTouch " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Logger.d("onStopTrackingTouch " + seekBar.getProgress());

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("resultCode=" + resultCode);
        if (data != null) {
            int color = (int) data.getExtras().getInt(Intent.ACTION_PICK);
            Logger.d(data.getExtras().get(Intent.ACTION_PICK).toString());
            Logger.d(data.getExtras().get("name").toString());
            Logger.d(data.getExtras().get("color").toString());
            if (requestCode == 0) {
                if(color == -1){
                    color = default_fontcolor;
                }
                textColor = color;
                int org = text1.getTextColors().getDefaultColor();
                textBtn.setText(getColorString(textColor));
                text1.setTextColor(Color.argb(Color.alpha(org),
                        Color.red(textColor), Color.green(textColor),
                        Color.blue(textColor)));
                text2.setTextColor(Color.argb(Color.alpha(org),
                        Color.red(textColor), Color.green(textColor),
                        Color.blue(textColor)));
            } else if (requestCode == 1) {
                if(color == -1){
                    color = default_backcolor;
                }
                backColor = color;
                int org = shape.getPaint().getColor();
                shape.getPaint().setStyle(Style.FILL);
                shape.getPaint().setColor(
                        Color.argb(Color.alpha(org), Color.red(backColor),
                                Color.green(backColor), Color.blue(backColor)));
                panelWidget.setBackgroundDrawable(shape.getCurrent());
                backBtn.setText(getColorString(backColor));
            }
        }
    }

    public String getColorString(int icolor) {
        int r = Color.red(icolor);
        int g = Color.green(icolor);
        int b = Color.blue(icolor);
        return "#" + toHexString(r, 2) + toHexString(g, 2) + toHexString(b, 2);
    }

    public String toHexString(int value, int n) {
        String s = Integer.toHexString(value);
        StringBuilder sb = new StringBuilder();
        for (int i = s.length(); i < n; i++) {
            sb.append("0");
        }
        sb.append(s);
        return sb.toString().toUpperCase();
    }
}

package jp.co.kayo.android.localplayer.db;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.util.Logger;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/***
 * このクラスはJustPlayerが独自に管理するデータへアクセスするためのクラスです
 * 
 * @author yokmama
 */
public class JustPlayerDatabaseHelper extends SQLiteOpenHelper {
    // データベースのバージョン（スキーマが変わったときにインクリメントする）
    private static final String DATABASE_NAME = "mydatabase.db";
    private static final int DATABASE_VERSION = 6;

    /***
     * デフォルトのコンストラクタ
     * 
     * @param context
     * @param databasename
     */
    public JustPlayerDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            // favorite
            if (!findTable(db, TableConsts.TBNAME_FAVORITE)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_FAVORITE)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(TableConsts.FAVORITE_ID).append(" LONG");
                sql.append(",").append(TableConsts.FAVORITE_TYPE)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            // orderlist
            if (!findTable(db, TableConsts.TBNAME_PLAYBACK)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_PLAYBACK).append(" (");
                sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(TableConsts.PLAYBACK_URI).append(" TEXT");
                sql.append(",").append(TableConsts.PLAYBACK_TITLE).append(" TEXT");
                sql.append(",").append(TableConsts.PLAYBACK_ARTIST).append(" TEXT");
                sql.append(",").append(TableConsts.PLAYBACK_ALBUM).append(" TEXT");
                sql.append(",").append(TableConsts.PLAYBACK_MEDIA_ID).append(" LONG");
                sql.append(",").append(TableConsts.PLAYBACK_ALBUM_KEY).append(" TEXT");
                sql.append(",").append(TableConsts.PLAYBACK_ARTIST_KEY).append(" TEXT");
                sql.append(",").append(TableConsts.PLAYBACK_DATA).append(" TEXT");
                sql.append(",").append(TableConsts.PLAYBACK_DURATION).append(" LONG");
                sql.append(",").append(TableConsts.PLAYBACK_ORDER).append(" LONG");
                sql.append(",").append(TableConsts.PLAYBACK_STATE).append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }
            if (!findTable(db, TableConsts.TBNAME_DOWNLOAD)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_DOWNLOAD)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(TableConsts.DOWNLOAD_ID).append(" LONG");
                sql.append(",").append(TableConsts.DOWNLOAD_MEDIA_ID)
                        .append(" LONG");
                sql.append(",").append(TableConsts.DOWNLOAD_TITLE)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_LOCAL_URI)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_REMOTE_URI)
                        .append(" TEXT");
                sql.append(",").append(TableConsts.DOWNLOAD_TYPE)
                        .append(" INTEGER");
                sql.append(",").append(TableConsts.DOWNLOAD_STATUS)
                        .append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }

            if (!findTable(db, TableConsts.TBNAME_BOOKMARK)) {
                StringBuilder sql = new StringBuilder();
                sql.append("CREATE TABLE ").append(TableConsts.TBNAME_BOOKMARK)
                        .append(" (");
                sql.append(BaseColumns._ID).append(
                        " INTEGER PRIMARY KEY AUTOINCREMENT");
                sql.append(",").append(TableConsts.BOOKMARK_MEDIA_ID).append(" LONG");
                sql.append(",").append(TableConsts.BOOKMARK_MEDIA_DURATION).append(" LONG");
                sql.append(",").append(TableConsts.BOOKMARK_TITLE).append(" TEXT");
                sql.append(",").append(TableConsts.BOOKMARK_HEAD).append(" LONG");
                sql.append(",").append(TableConsts.BOOKMARK_DURATION).append(" LONG");
                sql.append(",").append(TableConsts.BOOKMARK_DATA).append(" TEXT");
                sql.append(",").append(TableConsts.FAVORITE_POINT).append(" INTEGER");
                sql.append(");");
                db.execSQL(sql.toString());
            }

            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Logger.d("Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_PLAYBACK);
        if(oldVersion < 2){
            db.execSQL("DROP TABLE IF EXISTS " + TableConsts.TBNAME_FAVORITE);
        }
        onCreate(db);
    }

    private boolean findTable(SQLiteDatabase db, String tbl) {

        StringBuilder where = new StringBuilder();
        where.append("type='table' and name='");
        where.append(tbl);
        where.append("'");

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables("sqlite_master"); // テーブル名
        Cursor cur = null;
        try {
            cur = qb.query(db, null, where.toString(), null, null, null, null,
                    null);
            if (cur != null) {
                if (cur.moveToFirst()) {
                    return true;
                }
            }
            return false;
        } finally {
            if (cur != null) {
                cur.close();
            }
        }
    }
}

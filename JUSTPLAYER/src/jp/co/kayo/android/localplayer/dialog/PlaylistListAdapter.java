package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.ColorSet;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.core.IndexCursorAdapter;
import jp.co.kayo.android.localplayer.core.ViewHolder;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ViewCache;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PlaylistListAdapter extends IndexCursorAdapter {
    LayoutInflater inflator;
    Context context;
    Handler handler = new Handler();
    java.text.DateFormat format;

    public PlaylistListAdapter(Context context, Cursor c, ViewCache viewCache) {
        super(context, c, true, viewCache, MediaConsts.AudioPlaylist.NAME);
        this.context = context;
        format = DateFormat.getMediumDateFormat(context);
    }

    public LayoutInflater getInflator(Context context) {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        String name = cursor.getString(cursor
                .getColumnIndex(MediaConsts.AudioPlaylist.NAME));
        long modified = cursor.getLong(cursor
                .getColumnIndex(MediaConsts.AudioPlaylist.DATE_MODIFIED));

        int pos = cursor.getPosition();
        holder.getText1().setText(Funcs.getTrack(pos + 1));
        holder.getText2().setText(name);
        holder.getText3().setText(format.format(modified));
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = getInflator(context).inflate(R.layout.playlist_list_row,
                parent, false);
        ViewHolder holder = new ViewHolder();
        holder.setText1((TextView) v.findViewById(R.id.textTrack));
        holder.setText2((TextView) v.findViewById(R.id.textName));
        holder.setText3((TextView) v.findViewById(R.id.textModified));
        v.setTag(holder);
        return v;
    }
}

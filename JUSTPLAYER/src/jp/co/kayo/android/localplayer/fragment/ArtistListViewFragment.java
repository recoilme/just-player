package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.TagEditActivity;
import jp.co.kayo.android.localplayer.adapter.ArtistListViewAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioAlbum;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioArtist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioMedia;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.FastOnTouchListener;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader.CallType;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

@SuppressLint("NewApi")
public class ArtistListViewFragment extends BaseListFragment implements
        ContentManager, LoaderCallbacks<Cursor>, OnScrollListener {
    private ViewCache viewcache;
    private MyPreferenceManager mPref;
    ArtistListViewAdapter mAdapter;
    ListView mListView;
    private AnActionModeOfEpicProportions mActionMode;

    boolean isReadFooter;
    Runnable mTask = null;
    MatrixCursor mcur;
    AsyncTask<Void, Void, Void> loadtask = null;
    boolean getall = false;

    @Override
    protected void messageHandle(int what, List<Integer> items) {
        if (items.size() > 0) {
            switch (what) {
            case SystemConsts.EVT_SELECT_RATING: {
                Long[] ids = new Long[items.size()];
                for (int i = 0; i < items.size(); i++) {
                    int pos = items.get(i);
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(pos);
                    if (selectedCursor != null) {
                        long artistId = selectedCursor.getLong(selectedCursor
                                .getColumnIndex(AudioArtist._ID));
                        ids[i] = artistId;
                    }
                }
                RatingDialog dlg = new RatingDialog(getActivity(),
                        TableConsts.FAVORITE_TYPE_ARTIST, ids, mHandler);
                dlg.show();
            }
                break;
            case SystemConsts.EVT_SELECT_ADD: {
                ArrayList<String> whereArgs = new ArrayList<String>();
                StringBuilder where = new StringBuilder();
                for (Integer i : items) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                    if (selectedCursor != null) {
                        String artistKey = selectedCursor
                                .getString(selectedCursor
                                        .getColumnIndex(AudioArtist.ARTIST_KEY));
                        if (where.length() > 0) {
                            where.append(" OR ");
                        }
                        where.append(MediaConsts.AudioMedia.ARTIST_KEY + " = ?");
                        whereArgs.add(artistKey);
                    }
                }

                AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(
                        getActivity(), getFragmentManager(), where.toString(),
                        whereArgs.toArray(new String[whereArgs.size()]),
                        CallType.TYPE_ADDDIALOG);
                task.execute();
            }
                break;
            case SystemConsts.EVT_SELECT_CLEARCACHE: {
                ArrayList<String> whereArgs = new ArrayList<String>();
                StringBuilder where = new StringBuilder();
                for (Integer i : items) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                    if (selectedCursor != null) {
                        String artistKey = selectedCursor
                                .getString(selectedCursor
                                        .getColumnIndex(AudioArtist.ARTIST_KEY));
                        if (where.length() > 0) {
                            where.append(" OR ");
                        }
                        where.append(MediaConsts.AudioMedia.ARTIST_KEY + " = ?");
                        whereArgs.add(artistKey);
                    }
                }
                Cursor cursor = null;
                try {
                    cursor = getActivity().getContentResolver().query(
                            MediaConsts.MEDIA_CONTENT_URI, null,
                            where.toString(),
                            whereArgs.toArray(new String[whereArgs.size()]),
                            null);
                    if (cursor != null && cursor.moveToFirst()) {
                        do {
                            long mediaId = cursor.getLong(cursor
                                    .getColumnIndex(AudioMedia._ID));
                            String data = cursor
                                    .getString(cursor
                                            .getColumnIndex(MediaConsts.AudioMedia.DATA));

                            ContentsUtils.clearMediaCache(getActivity(), data);

                            ContentValues values = new ContentValues();
                            values.put(TableConsts.AUDIO_CACHE_FILE,
                                    (String) null);
                            getActivity().getContentResolver().update(
                                    ContentUris.withAppendedId(
                                            MediaConsts.MEDIA_CONTENT_URI,
                                            mediaId), values, null, null);
                        } while (cursor.moveToNext());
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
                break;
            case SystemConsts.EVT_SELECT_EDIT: {
                String artistKey = null;
                long[] ids = new long[items.size()];
                for (int i = 0; i < items.size(); i++) {
                    int pos = items.get(i);
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(pos);
                    if (selectedCursor != null) {
                        long artistId = selectedCursor.getLong(selectedCursor
                                .getColumnIndex(AudioArtist._ID));
                        ids[i] = artistId;
                        artistKey = selectedCursor.getString(selectedCursor
                                .getColumnIndex(AudioArtist.ARTIST_KEY));
                    }
                }
                Intent intent = new Intent(getActivity(), TagEditActivity.class);
                intent.putExtra(SystemConsts.KEY_EDITTYPE,
                        TagEditActivity.ARTIST);
                if (ids.length < 2) {
                    intent.putExtra(SystemConsts.KEY_IMAGEKEY,
                            Funcs.getAlbumKey(null, artistKey));
                }
                intent.putExtra(SystemConsts.KEY_EDITKEYLIST, ids);
                getActivity().startActivityForResult(intent,
                        SystemConsts.REQUEST_TAGEDIT);
            }
                break;
            case SystemConsts.EVT_SELECT_CHECKALL: {
                for (int i = 0; i < mListView.getCount(); i++) {
                    mListView.setItemChecked(i, true);
                }
            }
                break;
            default: {
                messageHandle(what, items.get(0));
            }
            }
        }
    }

    protected void messageHandle(int what, int selectedPosition) {
        Cursor selectedCursor = (Cursor) mAdapter.getItem(selectedPosition);
        if (selectedCursor != null) {
            switch (what) {
            case SystemConsts.EVT_SELECT_MORE: {
                String artist = selectedCursor.getString(selectedCursor
                        .getColumnIndex(AudioArtist.ARTIST));
                if (artist != null) {
                    showInfoDialog(null, artist, null);
                }
            }
                break;
            default: {
            }
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        mPref = new MyPreferenceManager(getActivity());
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.artistart_list_view, container,
                false);

        mListView = (ListView) root.findViewById(android.R.id.list);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);
        mListView.setOnScrollListener(this);
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_selectall))
                    .setIcon(R.drawable.ic_menu_selectall)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_rating))
                    .setIcon(R.drawable.ic_menu_star)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            menu.add(getString(R.string.txt_web_more))
                    .setIcon(R.drawable.ic_menu_wikipedia)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_delete)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }

            return true;
        }

    };

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (mActionMode.hasMenu()) {
            mActionMode.onItemClick(l, v, position, id);
        } else {
            Cursor cursor = (Cursor) getListAdapter().getItem(position);
            if (cursor != null) {
                String artist_key = cursor.getString(cursor
                        .getColumnIndex(AudioArtist.ARTIST_KEY));
                String artist = cursor.getString(cursor
                        .getColumnIndex(AudioArtist.ARTIST));
                int max = cursor.getInt(cursor
                        .getColumnIndex(AudioArtist.NUMBER_OF_TRACKS));

                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.replace(
                        getId(),
                        jp.co.kayo.android.localplayer.fragment.ArtistListFragment
                                .createFragment(artist_key, artist, max,
                                        FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            }
        }
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        return R.layout.artistart_list_view;
    }

    public String getSortOrder() {
        return MediaConsts.AudioArtist.ARTIST;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = getSortOrder();
        if (mAdapter == null) {
            mAdapter = new ArtistListViewAdapter(getActivity(), null, viewcache);
            getListView().setOnTouchListener(
                    new FastOnTouchListener(new Handler(), mAdapter));
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        getall = false;
        showProgressBar();
        return new CursorLoader(getActivity(), MediaConsts.ARTIST_CONTENT_URI,
                null, selection, selectionArgs, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            hideProgressBar();
            if (mAdapter != null && data != null && !data.isClosed()) {
                if (data == null || data.isClosed()) {
                    if (data == null)
                        mAdapter.swapCursor(null);
                    invisibleProgressBar();
                    return;
                }
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    int count = data.getCount();
                    try {
                        mcur = new MatrixCursor(data.getColumnNames(),
                                data.getCount());
                        if (count > 0) {
                            data.moveToFirst();
                            do {
                                ArrayList<Object> values = new ArrayList<Object>();
                                for (int i = 0; i < mcur.getColumnCount(); i++) {
                                    if (AudioArtist.NUMBER_OF_ALBUMS
                                            .equals(mcur.getColumnName(i))
                                            || AudioArtist.NUMBER_OF_TRACKS
                                                    .equals(mcur
                                                            .getColumnName(i))) {
                                        values.add(data.getInt(i));
                                    } else {
                                        values.add(data.getString(i));
                                    }
                                }
                                mcur.addRow(values.toArray(new Object[values
                                        .size()]));
                            } while (data.moveToNext());
                            Cursor cur = mAdapter.swapCursor(mcur);
                        }

                    } finally {
                        if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                            invisibleProgressBar();
                        }
                        if (data != null) {
                            data.close();
                        }
                    }
                } else {
                    Cursor cur = mAdapter.swapCursor(data);
                    invisibleProgressBar();
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
        mcur = null;
    }

    public static final String[] fetchcols = new String[] { AudioArtist._ID,
            AudioArtist.ARTIST, AudioArtist.ARTIST_KEY,
            AudioArtist.NUMBER_OF_ALBUMS, AudioArtist.NUMBER_OF_TRACKS };

    private void invisibleProgressBar() {
        getall = true;
        mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getCount();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_SHOWPROGRESS);
                                cursor = getActivity()
                                        .getContentResolver()
                                        .query(MediaConsts.ARTIST_CONTENT_URI,
                                                fetchcols,
                                                "LIMIT ? AND OFFSET ?",
                                                new String[] {
                                                        Integer.toString(SystemConsts.DEFAULT_FETCH_LIMIT),
                                                        Long.toString(current) },
                                                null);
                                int count = cursor.getCount();
                                if (mcur != null && count > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                if (AudioArtist.NUMBER_OF_ALBUMS
                                                        .equals(colname)
                                                        || AudioArtist.NUMBER_OF_TRACKS
                                                                .equals(colname)) {
                                                    values.add(cursor
                                                            .getInt(colid));
                                                } else {
                                                    values.add(cursor
                                                            .getString(colid));
                                                }
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());

                                    if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                        invisibleProgressBar();
                                    }
                                } else {
                                    if (count < 1) {
                                        invisibleProgressBar();
                                    }
                                }
                            } finally {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
        if (mcur != null) {
            mcur.addRow(values);
        }
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_artist_name);
    }
}

package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.adapter.GenresListViewAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.TableConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioGenres;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.dialog.RatingDialog;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.provider.ContentsUtils;
import jp.co.kayo.android.localplayer.service.StreamCacherServer;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader.CallType;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class GenresListViewFragment extends BaseListFragment implements
        ContentManager, LoaderCallbacks<Cursor>, OnScrollListener {
    private MyPreferenceManager mPref;
    private ViewCache mViewcache;
    private GenresListViewAdapter mAdapter;
    ListView mListView;
    private AnActionModeOfEpicProportions mActionMode;

    boolean isReadFooter;
    Runnable mTask = null;
    MatrixCursor mcur;
    AsyncTask<Void, Void, Void> loadtask = null;
    boolean getall = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new MyPreferenceManager(getActivity());
        mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                SystemConsts.TAG_CACHE);
        if (savedInstanceState != null) {
            getall = savedInstanceState.getBoolean("getall");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("getall", getall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.genres_grid_view, container,
                false);

        mListView = (ListView) root.findViewById(android.R.id.list);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);
        mListView.setOnScrollListener(this);
        isReadFooter = ContentsUtils.isNoCacheAction(mPref);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(getFragmentId(), null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (mActionMode.hasMenu()) {
            mActionMode.onItemClick(l, v, position, id);
        } else {
            Cursor cursor = (Cursor) getListAdapter().getItem(position);
            if (cursor != null) {
                String genres_col = AudioGenres.GENRES_KEY;
                int col = cursor.getColumnIndex(genres_col);
                if (col == -1) {
                    genres_col = AudioGenres._ID;
                    col = cursor.getColumnIndex(genres_col);
                }
                String genres_id = cursor.getString(col);
                String genres_name = cursor.getString(cursor
                        .getColumnIndex(AudioGenres.NAME));
                if (genres_id != null) {

                    getFragmentManager().popBackStack(
                            SystemConsts.TAG_SUBFRAGMENT,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    FragmentTransaction t = getFragmentManager()
                            .beginTransaction();
                    AnimationHelper.setFragmentToPlayBack(mPref, t);
                    t.replace(getId(), GenresListFragment.createFragment(
                            genres_id, genres_name,
                            FragmentUtils.cloneBundle(this)));
                    t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                    t.hide(this);
                    t.commit();
                } else {
                    Toast.makeText(getActivity(),
                            getString(R.string.txt_cannotopen_data),
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_selectall))
                    .setIcon(R.drawable.ic_menu_selectall)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            
            if (!ContentsUtils.isSDCard(mPref)) {
                menu.add(getString(R.string.sub_mnu_clearcache))
                        .setIcon(R.drawable.ic_menu_delete)
                        .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
            }
            return true;
        }
    }

    @Override
    protected void messageHandle(int what, List<Integer> items) {
        if (items.size() > 0) {
            switch (what) {
            case SystemConsts.EVT_SELECT_ADD: {
                ArrayList<String> whereArgs = new ArrayList<String>();
                StringBuilder where = new StringBuilder();
                String audio_id_col = null;
                for (Integer i : items) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                    if (selectedCursor != null) {
                        if (audio_id_col == null) {
                            audio_id_col = MediaConsts.AudioGenresMember._ID;
                            int audio_id_index = selectedCursor
                                    .getColumnIndex(MediaConsts.AudioGenresMember.AUDIO_ID);
                            if (audio_id_index != -1) {
                                audio_id_col = MediaConsts.AudioGenresMember.AUDIO_ID;
                            }
                        }

                        int col = selectedCursor
                                .getColumnIndex(AudioGenres.GENRES_KEY);
                        if (col == -1) {
                            col = selectedCursor
                                    .getColumnIndex(AudioGenres._ID);
                        }
                        String genresKey = selectedCursor.getString(col);

                        if (where.length() > 0) {
                            where.append(" OR ");
                        }
                        where.append(MediaConsts.AudioGenresMember.GENRE_ID
                                + " = ?");
                        whereArgs.add(genresKey);
                    }
                }

                AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(
                        getActivity(), getFragmentManager(),
                        MediaConsts.GENRES_MEMBER_CONTENT_URI,
                        MediaConsts.AudioGenresMember._ID,
                        MediaConsts.AudioGenresMember.GENRE_ID + " = ?",
                        whereArgs.toArray(new String[whereArgs.size()]),
                        MediaConsts.AudioGenresMember.DEFAULT_SORT_ORDER,
                        CallType.TYPE_ADDDIALOG);
                task.execute();
            }
                break;
            case SystemConsts.EVT_SELECT_CLEARCACHE: {
                for (Integer i : items) {
                    Cursor selectedCursor = (Cursor) mAdapter.getItem(i);
                    if (selectedCursor != null) {
                        int col = selectedCursor
                                .getColumnIndex(AudioGenres.GENRES_KEY);
                        if (col == -1) {
                            col = selectedCursor
                                    .getColumnIndex(AudioGenres._ID);
                        }
                        String genresKey = selectedCursor.getString(col);

                        Cursor cursor = null;
                        try {
                            cursor = getActivity()
                                    .getContentResolver()
                                    .query(MediaConsts.GENRES_MEMBER_CONTENT_URI,
                                            null,
                                            MediaConsts.AudioGenresMember.GENRE_ID
                                                    + " = ?",
                                            new String[] { genresKey },
                                            MediaConsts.AudioGenresMember.DEFAULT_SORT_ORDER);
                            if (cursor != null && cursor.moveToFirst()) {
                                do {
                                    String title = cursor
                                            .getString(cursor
                                                    .getColumnIndex(MediaConsts.AudioMedia.TITLE));
                                    String data = cursor
                                            .getString(cursor
                                                    .getColumnIndex(MediaConsts.AudioMedia.DATA));
                                    File file = StreamCacherServer
                                            .getCacheFile(getActivity(), data);
                                    if (file != null && file.exists()) {
                                        file.delete();
                                        Toast.makeText(
                                                getActivity(),
                                                getString(R.string.txt_action_deletecache)
                                                        + "(" + title + ")",
                                                Toast.LENGTH_SHORT).show();
                                        mAdapter.notifyDataSetChanged();
                                    }
                                } while (cursor.moveToNext());
                            }
                        } finally {
                            if (cursor != null) {
                                cursor.close();
                            }
                        }
                    }
                }
                datasetChanged();
                // reload();
            }
                break;
            case SystemConsts.EVT_SELECT_CHECKALL: {
                for (int i = 0; i < mListView.getCount(); i++) {
                    mListView.setItemChecked(i, true);
                }
            }
                break;
            }
        }
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void changedMedia() {

    }

    @Override
    public void release() {
        // TODO Auto-generated method stub

    }

    @Override
    public int getFragmentId() {
        return R.layout.genres_grid_view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = null;
        String[] selectionArgs = null;
        String sortOrder = null;
        sortOrder = AudioGenres.NAME;

        if (mAdapter == null) {
            mAdapter = new GenresListViewAdapter(getActivity(), mViewcache,
                    null);
            setListAdapter(mAdapter);
        } else {
            Cursor cur = mAdapter.swapCursor(null);
            if (cur != null) {
                cur.close();
            }
        }

        getall = false;
        showProgressBar();
        return new CursorLoader(getActivity(), MediaConsts.GENRES_CONTENT_URI,
                null, selection, selectionArgs, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            hideProgressBar();
            if (mAdapter != null && data != null && !data.isClosed()) {
                if (data == null || data.isClosed()) {
                    if (data == null)
                        mAdapter.swapCursor(null);
                    invisibleProgressBar();
                    return;
                }
                if (ContentsUtils.isNoCacheAction(mPref)) {
                    int count = data.getCount();
                    try {
                        mcur = new MatrixCursor(data.getColumnNames(),
                                data.getCount());
                        if (count <= 0) {
                            invisibleProgressBar();
                        }
                        if (count > 0) {
                            data.moveToFirst();
                            do {
                                ArrayList<Object> values = new ArrayList<Object>();
                                for (int i = 0; i < mcur.getColumnCount(); i++) {
                                    values.add(data.getString(i));
                                }
                                mcur.addRow(values.toArray(new Object[values
                                        .size()]));
                            } while (data.moveToNext());
                            Cursor cur = mAdapter.swapCursor(mcur);
                        }

                    } finally {
                        if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                            invisibleProgressBar();
                        }
                        if (data != null) {
                            data.close();
                        }
                    }
                } else {
                    Cursor cur = mAdapter.swapCursor(data);
                    invisibleProgressBar();
                }
            }
        } finally {
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgressBar();
        mcur = null;
    }

    private void invisibleProgressBar() {
        getall = true;
        mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
            int visibleItemCount, int totalItemCount) {
        if (mAdapter == null || loadtask != null) {
            return;
        }
        if (isReadFooter) {
            if (!getall && totalItemCount > 0
                    && totalItemCount == (firstVisibleItem + visibleItemCount)) {
                final int current = mAdapter.getCount();
                if (current > 0) {
                    loadtask = new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            Cursor cursor = null;
                            try {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_SHOWPROGRESS);
                                cursor = getActivity()
                                        .getContentResolver()
                                        .query(MediaConsts.GENRES_CONTENT_URI,
                                                null,
                                                "LIMIT ? AND OFFSET ?",
                                                new String[] {
                                                        Integer.toString(SystemConsts.DEFAULT_FETCH_LIMIT),
                                                        Long.toString(current) },
                                                null);
                                int count = cursor.getCount();
                                if (mcur != null && count > 0) {
                                    cursor.moveToFirst();
                                    do {
                                        ArrayList<Object> values = new ArrayList<Object>();
                                        for (int i = 0; i < mcur
                                                .getColumnCount(); i++) {
                                            String colname = mcur
                                                    .getColumnName(i);
                                            int colid = cursor
                                                    .getColumnIndex(colname);
                                            if (colid != -1) {
                                                values.add(cursor
                                                        .getString(colid));
                                            }
                                        }
                                        mHandler.sendMessage(mHandler
                                                .obtainMessage(
                                                        SystemConsts.ACT_ADDROW,
                                                        values.toArray(new Object[values
                                                                .size()])));
                                    } while (cursor.moveToNext());

                                    if (count < SystemConsts.DEFAULT_FETCH_LIMIT) {
                                        invisibleProgressBar();
                                    }
                                } else {
                                    if (count < 1) {
                                        invisibleProgressBar();
                                    }
                                }
                            } finally {
                                mHandler.sendEmptyMessage(SystemConsts.ACT_HIDEPROGRESS);
                                if (cursor != null) {
                                    cursor.close();
                                }
                                mHandler.sendEmptyMessage(SystemConsts.ACT_NOTIFYDATASETCHANGED);
                                loadtask = null;
                            }

                            return null;
                        }
                    };
                    loadtask.execute((Void) null);
                }
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
        if (mcur != null) {
            mcur.addRow(values);
        }
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_genres_name);
    }
}

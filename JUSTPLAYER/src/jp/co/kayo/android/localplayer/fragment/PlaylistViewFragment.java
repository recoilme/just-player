package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import jp.co.kayo.android.localplayer.BaseListFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.adapter.PlaylistListAdapter;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylist;
import jp.co.kayo.android.localplayer.consts.MediaConsts.AudioPlaylistMember;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.core.ContextMenuFragment;
import jp.co.kayo.android.localplayer.dialog.NewPlaylistDialog;
import jp.co.kayo.android.localplayer.menu.MultipleChoiceMediaContentActionCallback;
import jp.co.kayo.android.localplayer.task.AsyncAddPlaylistTask;
import jp.co.kayo.android.localplayer.util.AnimationHelper;
import jp.co.kayo.android.localplayer.util.FragmentUtils;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfo;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader.CallType;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

@SuppressLint("NewApi")
public class PlaylistViewFragment extends BaseListFragment implements
        ContentManager, LoaderCallbacks<List<PlaylistInfo>>, OnScrollListener {
    private MyPreferenceManager mPref;
    private ViewCache mViewcache;
    private PlaylistListAdapter mAdapter;
    Runnable mTask = null;
    ListView mListView;
    private AnActionModeOfEpicProportions mActionMode;

    String[] fetchcols = new String[] { AudioPlaylist._ID, AudioPlaylist.NAME,
            AudioPlaylist.PLAYLIST_KEY, AudioPlaylist.DATE_ADDED,
            AudioPlaylist.DATE_MODIFIED };

    ViewCache getCache() {
        if (mViewcache == null) {
            mViewcache = (ViewCache) getFragmentManager().findFragmentByTag(
                    SystemConsts.TAG_CACHE);
        }
        return mViewcache;
    }

    @Override
    public void hideMenu() {
        if (mActionMode != null) {
            mActionMode.cancelActionMode();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.playlist_grid_view, null, false);

        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setOnScrollListener(this);
        mActionMode = new AnActionModeOfEpicProportions(getActivity(),
                mListView, mHandler);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new MyPreferenceManager(getActivity());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(getFragmentId(), null, this);
        if (savedInstanceState != null && getFragmentManager() != null) {
            if (getFragmentManager().getBackStackEntryCount() > 0) {
                FragmentTransaction t = getFragmentManager().beginTransaction();
                t.hide(this);
                t.commit();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getLoaderManager().destroyLoader(getFragmentId());
    }

    @Override
    public Loader<List<PlaylistInfo>> onCreateLoader(int arg0, Bundle arg1) {

        PlaylistInfoLoader loader = new PlaylistInfoLoader(getActivity(),
                CallType.TYPE_PLAYLIST);
        loader.forceLoad();
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<PlaylistInfo>> arg0,
            List<PlaylistInfo> arg1) {
        mAdapter = new PlaylistListAdapter(getActivity(), arg1, getCache());
        setListAdapter(mAdapter);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<List<PlaylistInfo>> arg0) {
        mAdapter = new PlaylistListAdapter(getActivity(),
                new ArrayList<PlaylistInfo>(), getCache());
        setListAdapter(mAdapter);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        PlaylistInfo item = mAdapter.getItem(position);
        if (item.id <= 0) {
            if (mActionMode.hasMenu()) {
                mActionMode.unselectItem(position);
            } else if (item.id == PlaylistInfoLoader.PLAYLIST_ADDNEW_ID) {
                NewPlaylistDialog dlg = new NewPlaylistDialog();
                dlg.show(getFragmentManager(), "NewPlayDialog");
            } else if (item.id == PlaylistInfoLoader.PLAYLIST_5START_ID) {
                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.replace(
                        getId(),
                        FavoritelistFragment.createFragment(5,
                                FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            } else if (item.id == PlaylistInfoLoader.PLAYLIST_4START_ID) {
                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.replace(
                        getId(),
                        FavoritelistFragment.createFragment(4,
                                FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            } else if (item.id == PlaylistInfoLoader.PLAYLIST_3START_ID) {
                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.replace(
                        getId(),
                        FavoritelistFragment.createFragment(3,
                                FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            }
        } else {
            if (mActionMode.hasMenu()) {
                mActionMode.onItemClick(l, v, position, id);
            } else {
                long playlistId = item.id;
                String playlistName = item.name;
                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.replace(getId(), PlaylistFragment.createFragment(playlistId,
                        playlistName, FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            }
        }
    }

    @Override
    public void reload() {
        getLoaderManager().restartLoader(getFragmentId(), null, this);
    }

    @Override
    public void release() {
    }

    @Override
    public void changedMedia() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_playlist_name);
    }

    @Override
    public boolean onBackPressed() {
        if (mActionMode != null && mActionMode.cancelActionMode()) {
            return true;
        }
        return false;
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    protected void datasetChanged() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void addRow(Object[] values) {
    }

    @Override
    protected void messageHandle(int what, List<Integer> items) {
        if (items.size() > 0) {
            switch (what) {
            case SystemConsts.EVT_SELECT_DEL:
            case SystemConsts.EVT_SELECT_ADD: {
                ArrayList<PlaylistInfo> selectedItems = new ArrayList<PlaylistInfo>();
                for (Integer i : items) {
                    PlaylistInfo item = (PlaylistInfo) mAdapter.getItem(i);
                    selectedItems.add(item);
                }
                messageHandle(what, selectedItems);
            }
                break;
            case SystemConsts.EVT_SELECT_EDIT: {
                PlaylistInfo selectedItem = (PlaylistInfo) mAdapter
                        .getItem(items.get(0));
                long playlistId = selectedItem.id;
                String name = selectedItem.name;

                getFragmentManager().popBackStack(SystemConsts.TAG_SUBFRAGMENT,
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction t = getFragmentManager().beginTransaction();
                AnimationHelper.setFragmentToPlayBack(mPref, t);
                t.replace(getId(), PlaylistEditFragment.createFragment(
                        playlistId, name, FragmentUtils.cloneBundle(this)));
                t.addToBackStack(SystemConsts.TAG_SUBFRAGMENT);
                t.hide(this);
                t.commit();
            }
                break;
            default: {
            }
            }
        }
    }

    protected void messageHandle(int what, ArrayList<PlaylistInfo> items) {
        switch (what) {
        case SystemConsts.EVT_SELECT_DEL: {
            deletePlaylist(items);
        }
            break;
        case SystemConsts.EVT_SELECT_ADD: {
            ArrayList<Long> ids = new ArrayList<Long>();
            for (int i = 0; i < items.size(); i++) {
                PlaylistInfo item = items.get(i);
                long playlistId = item.id;
                Cursor cursor = null;
                try {
                    cursor = getActivity().getContentResolver().query(
                            ContentUris.withAppendedId(
                                    MediaConsts.PLAYLIST_CONTENT_URI,
                                    playlistId),
                            new String[] { AudioPlaylistMember.AUDIO_ID },
                            null, null, AudioPlaylistMember.PLAY_ORDER);
                    if (cursor != null && cursor.moveToFirst()) {
                        int n = cursor.getCount();
                        if (n > 0) {
                            do {
                                long id = cursor.getLong(0);
                                ids.add(id);
                            } while (cursor.moveToNext());
                        }
                    }
                } finally {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
            AsyncAddPlaylistTask task = new AsyncAddPlaylistTask(getActivity(),
                    getFragmentManager(), ids, CallType.TYPE_ADDDIALOG);
            task.execute();
        }
            break;
        }

    }

    private void deletePlaylist(final ArrayList<PlaylistInfo> items) {
        final ContentResolver resolver = getActivity().getContentResolver();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.lb_confirm));
        builder.setMessage(String.format(
                getString(R.string.fmt_remove_orderlist), items.size()));
        builder.setPositiveButton(getString(R.string.lb_ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for (int i = 0; i < items.size(); i++) {
                            PlaylistInfo item = items.get(i);
                            resolver.delete(ContentUris.withAppendedId(
                                    MediaConsts.PLAYLIST_CONTENT_URI, item.id),
                                    null, null);
                            resolver.delete(MediaConsts.PLAYLIST_CONTENT_URI,
                                    AudioPlaylist._ID + " = ?",
                                    new String[] { Long.toString(item.id) });
                        }
                        reload();
                    }
                });
        builder.setNegativeButton(getString(R.string.lb_cancel), null);
        builder.setCancelable(true);
        AlertDialog dlg = builder.create();
        dlg.show();
    }

    private final class AnActionModeOfEpicProportions extends
            MultipleChoiceMediaContentActionCallback {

        public AnActionModeOfEpicProportions(Context context,
                ListView listView, Handler handler) {
            super(context, listView, handler);
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {

            menu.add(getString(R.string.sub_mnu_order))
                    .setIcon(R.drawable.ic_menu_btn_add)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_remove))
                    .setIcon(R.drawable.ic_menu_remove)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            menu.add(getString(R.string.sub_mnu_edit))
                    .setIcon(R.drawable.ic_menu_strenc)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

            return true;
        }

    }

    @Override
    public int getFragmentId() {
        return R.layout.playlist_grid_view;
    }

    @Override
    public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Logger.d("onScrollStateChanged:" + scrollState);
        long time = mPref.getHideTime();
        if (time > 0) {
            if (scrollState == 1) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            } else if (scrollState == 0) {
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }

    };

}

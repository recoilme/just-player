package jp.co.kayo.android.localplayer.fragment;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Calendar;
import java.util.Date;

import com.google.ads.AdRequest;
import com.google.ads.AdView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import jp.co.kayo.android.localplayer.BaseFragment;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import jp.co.kayo.android.localplayer.core.ContentManager;
import jp.co.kayo.android.localplayer.util.MyPreferenceManager;

public class QuickHelpFragment extends BaseFragment implements ContentManager, OnTouchListener{
    private Date srcDate = Calendar.getInstance().getTime();
    private AdView mAdView;
    private Runnable mTask = null;
    MyPreferenceManager mPref;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPref = new MyPreferenceManager(getActivity());
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.quickhelp_view, container,
                false);
        
        WebView webview = (WebView)root.findViewById(R.id.helpView);

        webview.loadUrl(getString(R.string.help_url)); 
        webview.setHorizontalScrollBarEnabled(false);
        webview.setOnTouchListener(this);
        
        mAdView = (AdView)root.findViewById(R.id.adView);
        if(mAdView!=null){
            mAdView.loadAd(new AdRequest());
        }
        
        return root;
    }

    
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.lb_tab_blank_name);
    }

    @Override
    public void reload() {
    }

    @Override
    public void release() {
    }

    @Override
    public void changedMedia() {
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        long time = mPref.getHideTime();
        if (time > 0) {
            if(event.getAction() == MotionEvent.ACTION_DOWN){
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                if (getFragmentManager() != null) {
                    ControlFragment control = (ControlFragment) getFragmentManager()
                            .findFragmentByTag(SystemConsts.TAG_CONTROL);
                    if (control != null) {
                        control.hideControl(false);
                    }
                }
            }
            else if(event.getAction() == MotionEvent.ACTION_UP){
                if (mTask != null) {
                    mHandler.removeCallbacks(mTask);
                    mTask = null;
                }
                mTask = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (getFragmentManager() != null) {
                                ControlFragment control = (ControlFragment) getFragmentManager()
                                        .findFragmentByTag(
                                                SystemConsts.TAG_CONTROL);
                                if (control != null) {
                                    control.showControl(false);
                                }
                            }
                        } finally {
                            mTask = null;
                        }
                    }
                };
                mHandler.postDelayed(mTask, time);
            }
        }
        return false;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void hideMenu() {
    }

    @Override
    public String selectSort() {
        return null;
    }

    @Override
    public int getFragmentId() {
        return 0;
    }
    
    @Override
    public void doSearchQuery(String queryString) {
    }
}

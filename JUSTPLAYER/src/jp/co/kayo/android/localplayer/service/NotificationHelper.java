package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import jp.co.kayo.android.localplayer.util.Logger;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;

public class NotificationHelper {
    private Service service;
    private NotificationManager mNM;
    private Method mSetForeground;
    private Method mStartForeground;
    private Method mStopForeground;
    private Object[] mSetForegroundArgs = new Object[1];
    private Object[] mStartForegroundArgs = new Object[2];
    private Object[] mStopForegroundArgs = new Object[1];
    

    private static final Class<?>[] mSetForegroundSignature = new Class[] {
        boolean.class
    };
    private static final Class<?>[] mStartForegroundSignature = new Class[] {
            int.class, Notification.class
    };
    private static final Class<?>[] mStopForegroundSignature = new Class[] {
        boolean.class
    };
    

    public NotificationHelper(Service service){
        this.service = service;
        mNM = (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            mStartForeground = service.getClass().getMethod("startForeground",
                    mStartForegroundSignature);
            mStopForeground = service.getClass().getMethod("stopForeground",
                    mStopForegroundSignature);
            return;
        } catch (NoSuchMethodException e) {
            // Running on an older platform.
            mStartForeground = mStopForeground = null;
        }
        try {
            mSetForeground = service.getClass().getMethod("setForeground",
                    mSetForegroundSignature);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(
                    "OS doesn't have Service.startForeground OR Service.setForeground!");
        }
    }
    
    NotificationManager getNotificationManager(){
        return mNM;
    }

    void invokeMethod(Method method, Object[] args) {
        try {
            method.invoke(service, args);
        } catch (InvocationTargetException e) {
            // Should not happen.
            Logger.e("Unable to invoke method", e);
        } catch (IllegalAccessException e) {
            // Should not happen.
            Logger.e("Unable to invoke method", e);
        }
    }

    /**
     * This is a wrapper around the new startForeground method, using the older
     * APIs if it is not available.
     */
    void startForegroundCompat(int id, Notification notification) {
        // If we have the new startForeground API, then use it.
        if (mStartForeground != null) {
            //service.startForeground(id, notification);
            mStartForegroundArgs[0] = Integer.valueOf(id);
            mStartForegroundArgs[1] = notification;
            invokeMethod(mStartForeground, mStartForegroundArgs);
            return;
        }

        // Fall back on the old API.
        mSetForegroundArgs[0] = Boolean.TRUE;
        invokeMethod(mSetForeground, mSetForegroundArgs);
        mNM.notify(id, notification);
    }

    /**
     * This is a wrapper around the new stopForeground method, using the older
     * APIs if it is not available.
     */
    void stopForegroundCompat(int id) {
        // If we have the new stopForeground API, then use it.
        if (mStopForeground != null) {
            mStopForegroundArgs[0] = Boolean.TRUE;
            invokeMethod(mStopForeground, mStopForegroundArgs);
            return;
        }

        // Fall back on the old API. Note to cancel BEFORE changing the
        // foreground state, since we could be killed at that point.
        mNM.cancel(id);
        mSetForegroundArgs[0] = Boolean.FALSE;
        invokeMethod(mSetForeground, mSetForegroundArgs);
    }    
    
}

package jp.co.kayo.android.localplayer.service;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.MainActivity2;
import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.appwidget.AppWidgetHelper;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class NotificationService extends Service {
    private float mX;
    private float mY;

    /** ビュー(画面全体に展開) */
    private View mView;
    private WindowManager mWindowManager;

    @Override
    public void onCreate() {
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        setTouchView();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && "Click".equals(intent.getAction())) {
            DisplayMetrics metrics = new DisplayMetrics();
            mWindowManager.getDefaultDisplay().getMetrics(metrics);
            
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            boolean keepNotify = pref.getBoolean("key.useKeepNotification", false);
            int statusview_id = keepNotify?R.layout.statusbar:R.layout.statusbar2;
            
            if(statusview_id == R.layout.statusbar){
                float x1 = metrics.scaledDensity * 0;
                float x2 = metrics.scaledDensity * 64 + x1;
                float height = metrics.scaledDensity * 64;
                float freespace = (float) metrics.widthPixels - x2;
                float sepsize = freespace / 4;

                float x3 = x2;
                float x4 = sepsize + x3;

                float x5 = x4;
                float x6 = sepsize + x5;

                float x7 = x6;
                float x8 = sepsize + x7;

                float x9 = x8;
                float x10 = sepsize + x9;

                if (mX >= x1 && mX < x2) {
                    // AlbumArt
                    Intent i = new Intent(this, MainActivity2.class);
                    i.setAction(SystemConsts.MAIN_ACITON_SHOWHOMW);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } else if (mX >= x3 && mX < x4) {
                    // rew
                    Intent i = createIntent(this, AppWidgetHelper.CALL_REW);
                    startService(i);
                } else if (mX >= x5 && mX < x6) {
                    // pause play
                    Intent i = createIntent(this, AppWidgetHelper.CALL_PLAY_PAUSE);
                    startService(i);
                } else if (mX >= x7 && mX < x8) {
                    // ff
                    Intent i = createIntent(this, AppWidgetHelper.CALL_FF);
                    startService(i);
                } else if (mX >= x9 && mX < x10) {
                    // close
                    Intent i = createIntent(this, AppWidgetHelper.CALL_STOP);
                    startService(i);
                }
            }else{
                float x1 = metrics.scaledDensity * 0;
                float x2 = metrics.scaledDensity * 64 + x1;
                float height = metrics.scaledDensity * 64;
                float window_width = (float) metrics.widthPixels;
    
                float x10 = window_width;
                float x9 = x10 - x2;
                
                float x8 = x9;
                float x7 = x8 - x2;
    
                if (mX >= x1 && mX < x2) {
                    // AlbumArt
                    Intent i = new Intent(this, MainActivity2.class);
                    i.setAction(SystemConsts.MAIN_ACITON_SHOWHOMW);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                } else if (mX >= x7 && mX < x8) {
                    // pause play
                    Intent i = createIntent(this, AppWidgetHelper.CALL_PLAY_PAUSE);
                    startService(i);
                } else if (mX >= x9 && mX < x10) {
                    // ff
                    Intent i = createIntent(this, AppWidgetHelper.CALL_FF);
                    startService(i);
                }
            }

        }

        return START_NOT_STICKY;
    }

    public void onDestroy() {
        mWindowManager.removeView(mView);
        mView = null;
    }

    private Intent createIntent(Context context, String action) {
        Intent i = new Intent(context, MediaPlayerService.class);
        i.setAction(action);
        return i;
    }

    /**
     * タッチイベントの起動
     */
    private void setTouchView() {
        if (mView == null) {
            mView = new View(this) {
                public boolean onTouchEvent(MotionEvent motionevent) {
                    // タップされたX座標を代入
                    mX = motionevent.getX();
                    mY = motionevent.getY();
                    return super.onTouchEvent(motionevent);
                }
            };
        }

        // 面倒なので画面の横幅決め打ち
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, PixelFormat.TRANSLUCENT);
        mWindowManager.addView(mView, params);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

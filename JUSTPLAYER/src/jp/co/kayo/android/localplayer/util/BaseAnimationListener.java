package jp.co.kayo.android.localplayer.util;

import jp.co.kayo.android.localplayer.BaseActivity;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class BaseAnimationListener implements AnimationListener {
    BaseActivity activity;
    
    public BaseAnimationListener(BaseActivity activity){
        this.activity = activity;
    }
    
    public BaseActivity getActivity(){
        return activity;
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        
    }

    @Override
    public void onAnimationStart(Animation animation) {
        
    }

}

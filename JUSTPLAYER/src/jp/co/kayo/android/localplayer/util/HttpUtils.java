package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class HttpUtils {

    public JSONObject getJSON(String path) throws JSONException, IOException {

        InputStream in = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(path.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(20000);
            conn.setReadTimeout(20000);
            int responseCode = conn.getResponseCode();
            if (responseCode >= 200 && responseCode <= 206) {
                in = conn.getInputStream();

                String jsonstr = new String(getByteArrayFromStream(in));
                Log.d("KobekonClient", jsonstr);
                return new JSONObject(jsonstr);
            }
            else {
                throw new IOException("URL Connection Error :" + responseCode);
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public JSONArray getJSONArray(String path) throws JSONException, IOException {

        InputStream in = null;
        HttpURLConnection conn = null;
        try {
            URL url = new URL(path.toString());
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(20000);
            conn.setReadTimeout(20000);
            int responseCode = conn.getResponseCode();
            if (responseCode >= 200 && responseCode <= 206) {
                in = conn.getInputStream();

                String jsonstr = new String(getByteArrayFromStream(in));
                return new JSONArray(jsonstr);
            }
            else {
                throw new IOException("URL Connection Error :" + responseCode);
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public byte[] getByteArrayFromStream(InputStream in) {
        byte[] line = new byte[512];
        byte[] result = null;
        ByteArrayOutputStream out = null;
        int size = 0;
        try {
            out = new ByteArrayOutputStream();
            BufferedInputStream bis = new BufferedInputStream(in);
            while (true) {
                size = bis.read(line);
                if (size < 0) {
                    break;
                }
                out.write(line, 0, size);
            }
            result = out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
            }
        }
        return result;
    }
}

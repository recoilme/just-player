package jp.co.kayo.android.localplayer.util;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.dialog.EncodeStringListDialog.EncodeStringItems;

import android.content.Context;
import android.util.Log;

public class StringEncode {

    public static String getEncodeString(Context context, String s, String enc)
            throws UnsupportedEncodingException {
        int slen = s.length();
        byte bytes[] = new byte[slen];
        for (int i = 0; i < slen; i++) {
            char c = s.charAt(i);
            bytes[i] = (byte) c;
        }
        String encstr = new String(bytes, enc);
        return encstr;
    }

    public static boolean isGarbled(String s) {
        if (s != null) {
            int slen = s.length();
            for (int i = 0; i < slen; i++) {
                char c = s.charAt(i);
                if ((c <= '\u007e') || // 英数字
                        (c == '\u00a5') || // \記号
                        (c == '\u203e') || // ~記号
                        (c >= '\uff61' && c <= '\uff9f') // 半角カナ
                ){
                    //文字化けしてないのでスルー
                }
                else{
                    return true;
                }
            }
        }
        return false;
    }

    public static ArrayList<EncodeStringItems> getEncodeStrings(
            Context context, String s) {
        ArrayList<EncodeStringItems> list = new ArrayList<EncodeStringItems>();
        if (s == null) {
            return list;
        }
        String[] encodinglist = context.getResources().getStringArray(
                R.array.encode_type_list);
        int slen = s.length();
        byte bytes[] = new byte[slen];
        for (int i = 0; i < slen; i++) {
            char c = s.charAt(i);
            if (c >= 0x100) {
                // Byte列になっていない
                // おそらく文字化けしていないので、全部返却する
                for (int j = 0; j < encodinglist.length; j++) {
                    EncodeStringItems item = new EncodeStringItems();
                    item.str = s;
                    item.enc = encodinglist[j];
                    list.add(item);
                }
                return list;
            }
            bytes[i] = (byte) c;
        }

        for (int i = 0; i < encodinglist.length; i++) {
            String encstr = null;
            try {
                Log.w("StringEncode", "ENCODE KIND encodelist[i]"
                        + encodinglist[i]);
                encstr = new String(bytes, encodinglist[i]);
                // 文字コードチェック
                boolean vld = true;
                for (int j = 0; j < encstr.length(); j++) {
                    char x = encstr.charAt(j);
                    // Log.w("StringEncode",
                    // "ENCODE SUCCESS code:" + Integer.toHexString(x));
                    if (x >= 0xfffd) {
                        // U+FFFD (REPLACEMENT CHARACTER)
                        // 文字化けが発生したので無効にする？ TODO
                        vld = false;
                        break;
                    }
                    if ((x >= 0x7F) && (x <= 0xA0)) {
                        vld = false;
                        break;
                    }
                }
                if (vld) {
                    Log.w("StringEncode", "ENCODE SUCCESS encstr:" + encstr);
                    EncodeStringItems item = new EncodeStringItems();
                    item.str = encstr;
                    item.enc = encodinglist[i];
                    list.add(item);
                } else {
                    Log.w("StringEncode", "ENCODE SUCCESS but INVALID encstr:"
                            + encstr);
                }
            } catch (UnsupportedEncodingException e) {
                Log.w("StringEncode", "ENCODE ERROR encodelist[i]:"
                        + encodinglist[i]);
            }
        }

        return list;
    }

    /*
     * private static final String encodinglist[] = { // "Shift_JIS", //
     * Shift-JIS、日本語 "windows-31j", // Windows 日本語 "windows-1250", // Windows 東欧
     * "windows-1251", // Windows キリル文字 "windows-1252", // Windows ラテン文字-1
     * "windows-1253", // Windows ギリシャ文字 "windows-1254", // Windows トルコ語
     * "windows-1257", // Windows バルト諸語 "TIS-620", // TIS620、タイ "windows-1255",
     * // Windows ヘブライ語 "windows-1256", // Windows アラビア語 "windows-1258", //
     * Windows ベトナム語
     * 
     * "ISO-8859-1", // ISO -8859-1、ラテンアルファベット No. 1 "ISO-8859-2", // ラテンアルファベット
     * No. 2 "ISO-8859-4", // ラテンアルファベット No. 4 "ISO-8859-5", // ラテン/キリル文字アルファベット
     * "ISO-8859-7", // ラテン/ギリシャ文字アルファベット (ISO-8859-7:2003) "ISO-8859-9", //
     * ラテンアルファベット No. 5 "ISO-8859-13", // ラテンアルファベット No. 7 "ISO-8859-15", //
     * ラテンアルファベット No. 9 "ISO-8859-3", // ラテンアルファベット No. 3 "ISO-8859-6", //
     * ラテン/アラビア語アルファベット "ISO-8859-8", // ラテン/ヘブライ語アルファベット "KOI8-R", //
     * KOI8-R、ロシア語 "KOI8-U", // KOI8-U、ウクライナ語
     * 
     * "Big5-HKSCS", // Big5 (香港の拡張付き)、中国語 (繁体字、2001 改訂を組み込み) "EUC-JP", // JISX
     * 0201、0208、0212、EUC エンコーディング、日本語 "EUC-KR", // KS C 5601、EUC エンコーディング、韓国語
     * "GB18030", // 中国語 (簡体字)、中華人民共和国標準 "GB2312", // GB2312、EUC エンコーディング、中国語
     * (簡体字) "GBK", // GBK、中国語 (簡体字)
     * 
     * // "IBM00858", // Cp850 の拡張機能でユーロ文字を含む // "IBM437", // MS-DOS
     * 米国、オーストラリア、ニュージーランド、南アフリカ // "IBM775", // PC バルト諸語 // "IBM850", // MS-DOS
     * ラテン文字-1 // "IBM852", // MS-DOS ラテン文字-2 // "IBM855", // IBM キリル文字 //
     * "IBM857", // IBM トルコ語 // "IBM862", // PC ヘブライ語 // "IBM866", // MS-DOS
     * ロシア語
     * 
     * // "US-ASCII", // American Standard Code for Information Interchange //
     * "UTF-8", // 8 ビット Unicode (UCS) Transformation Format // "UTF-16", // 16
     * ビット Unicode (UCS) Transformation // Format、オプションのバイト順マークによって識別されるバイト順 //
     * "UTF-16BE", // 16 ビット Unicode (UCS) Transformation //
     * Format、ビッグエンディアンバイト順 // "UTF-16LE", // 16 ビット Unicode (UCS)
     * Transformation // Format、リトルエンディアンバイト順 // "UTF-32", // 32 ビット Unicode
     * (UCS) Transformation // Format、オプションのバイト順マークによって識別されるバイト順 // "UTF-32BE",
     * // 32 ビット Unicode (UCS) Transformation // Format、ビッグエンディアンバイト順 //
     * "UTF-32LE", // 32 ビット Unicode (UCS) Transformation //
     * Format、リトルエンディアンバイト順 // "x-UTF-32BE-BOM", // 32 ビット Unicode (UCS)
     * Transformation // Format、ビッグエンディアンバイト順、バイト順マーク付き // "x-UTF-32LE-BOM", //
     * 32 ビット Unicode (UCS) Transformation // Format、リトルエンディアンバイト順、バイト順マーク付き //
     * "x-IBM737", // PC ギリシャ文字 // "x-IBM874", // IBM タイ // "x-UTF-16LE-BOM", //
     * 16 ビット Unicode (UCS) Transformation // Format、リトルエンディアンバイト順、バイト順マーク付き //
     * "Big5", // Big5、中国語 (繁体字)
     * 
     * // EBICDIC "IBM-Thai", // IBM タイ拡張 SBCS "IBM01140", // Cp037
     * の拡張機能でユーロ文字を含む "IBM01141", // Cp273 の拡張機能でユーロ文字を含む "IBM01142", // Cp277
     * の拡張機能でユーロ文字を含む "IBM01143", // Cp278 の拡張機能でユーロ文字を含む "IBM01144", // Cp280
     * の拡張機能でユーロ文字を含む "IBM01145", // Cp284 の拡張機能でユーロ文字を含む "IBM01146", // Cp285
     * の拡張機能でユーロ文字を含む "IBM01147", // Cp297 の拡張機能でユーロ文字を含む "IBM01148", // Cp500
     * の拡張機能でユーロ文字を含む "IBM01149", // Cp871 の拡張機能でユーロ文字を含む "IBM037", // 米国、カナダ (2
     * か国語、フランス語)、オランダ、 ポルトガル、ブラジル、オーストラリア "IBM1026", // IBM ラテン文字-5、トルコ
     * "IBM1047", // ラテン文字-1 (EBCDIC ホスト用) "IBM273", // IBM オーストリア、ドイツ "IBM277",
     * // IBM デンマーク、ノルウェー "IBM278", // IBM フィンランド、スウェーデン "IBM280", // IBM イタリア
     * "IBM284", // IBM カタロニア語/スペイン、スペイン語圏ラテンアメリカ "IBM285", // IBM 英国、アイルランド
     * "IBM297", // IBM フランス "IBM420", // IBM アラビア語 "IBM424", // IBM ヘブライ語
     * "IBM500", // EBCDIC 500V1 "IBM860", // MS-DOS ポルトガル語 "IBM861", // MS-DOS
     * アイスランド語 "IBM863", // MS-DOS カナダ系フランス語 "IBM864", // PC アラビア語 "IBM865", //
     * MS-DOS 北欧 "IBM868", // MS-DOS パキスタン "IBM869", // IBM 近代ギリシャ語 "IBM870", //
     * IBM 多言語ラテン文字-2 "IBM871", // IBM アイスランド "IBM918", // IBM パキスタン (ウルドゥー語)
     * "ISO-2022-CN", // ISO 2022 CN 形式の GB2312 および CNS11643、簡体字および繁体字中国語
     * (Unicode への変換のみ) "ISO-2022-JP", // ISO 2022 形式の JIS X 0201、0208、日本語
     * "ISO-2022-KR", // ISO 2022 KR、韓国語 "JIS_X0201", // JIS X 0201
     * "JIS_X0212-1990", // JIS X 0212 "x-Big5_Solaris", // Big5 (Solaris
     * zh_TW.BIG5 ロケール用の 7 つの追加 Hanzi // 表意文字マッピング付き) "x-euc-jp-linux", // JISX
     * 0201、0208、EUC エンコーディング、日本語 "x-EUC-TW", // CNS11643 (Plane 1-7.15)、EUC
     * エンコーディング、中国語 (繁体字) "x-eucJP-Open", // JISX 0201、0208、0212、EUC
     * エンコーディング、日本語 "x-IBM1006", // IBM AIX パキスタン (ウルドゥー語) "x-IBM1025", // IBM
     * 多言語キリル文字: ブルガリア、ボスニア、ヘルツェゴビナ、マケドニア // (旧ユーゴスラビアマケドニア共和国) "x-IBM1046", //
     * IBM アラビア語 - Windows "x-IBM1097", // IBM イラン (現代ペルシャ語)/ペルシャ語 "x-IBM1098",
     * // IBM イラン (現代ペルシャ語)/ペルシャ語 (PC) "x-IBM1112", // IBM ラトビア、リトアニア
     * "x-IBM1122", // IBM エストニア "x-IBM1123", // IBM ウクライナ "x-IBM1124", // IBM
     * AIX ウクライナ "x-IBM1381", // IBM OS/2、DOS 中国 (中華人民共和国) "x-IBM1383", // IBM
     * AIX 中国 (中華人民共和国) "x-IBM33722", // IBM-eucJP - 日本語 (5050 のスーパーセット)
     * "x-IBM834", // IBM EBCDIC DBCS 専用 韓国語 "x-IBM856", // IBM ヘブライ語
     * "x-IBM875", // IBM ギリシャ語 "x-IBM921", // IBM ラトビア、リトアニア (AIX、DOS)
     * "x-IBM922", // IBM エストニア (AIX、DOS) "x-IBM930", // UDC 4370
     * 文字を含む日本語カタカナ漢字、5026 のスーパーセット "x-IBM933", // UDC 1880 文字を含む韓国語、5029
     * のスーパーセット "x-IBM935", // UDC 1880 文字を含む簡体字中国語ホスト、5031 のスーパーセット "x-IBM937",
     * // UDC 6204 文字を含む繁体字中国語ホスト、5033 のスーパーセット "x-IBM939", // UDC 4370
     * 文字を含む日本語ラテン文字漢字、5035 のスーパーセット "x-IBM942", // IBM OS/2 日本語、Cp932 のスーパーセット
     * "x-IBM942C", // Cp942 の拡張機能 "x-IBM943", // IBM OS/2 日本語、Cp932 および
     * Shift-JIS のスーパーセット "x-IBM943C", // Cp943 の拡張機能 "x-IBM948", // OS/2 中国語
     * (台湾)、938 のスーパーセット "x-IBM949", // PC 韓国語 "x-IBM949C", // Cp949 の拡張機能
     * "x-IBM950", // PC 中国語 (香港、台湾) "x-IBM964", // AIX 中国語 (台湾) "x-IBM970", //
     * AIX 韓国語 "x-ISCII91", // インド語派 ISCII91 エンコーディング "x-ISO2022-CN-CNS", // ISO
     * 2022 CN 形式の CNS11643、繁体字中国語 (Unicode // からの変換のみ) "x-ISO2022-CN-GB", //
     * ISO 2022 CN 形式の GB2312、簡体字中国語 (Unicode // からの変換のみ) "x-iso-8859-11", //
     * ラテン/タイ語アルファベット "x-JIS0208", // JIS X 0208 "x-JISAutoDetect", //
     * Shift-JIS、EUC-JP、ISO 2022 JP の検出および変換 (Unicode // への変換のみ) "x-Johab", //
     * 韓国語、Johab 文字セット "x-MacArabic", // Macintosh アラビア語 "x-MacCentralEurope",
     * // Macintosh ラテン文字-2 "x-MacCroatian", // Macintosh クロアチア語
     * "x-MacCyrillic", // Macintosh キリル文字 "x-MacDingbat", // Macintosh Dingbat
     * "x-MacGreek", // Macintosh ギリシャ語 "x-MacHebrew", // Macintosh ヘブライ語
     * "x-MacIceland", // Macintosh アイスランド語 "x-MacRoman", // Macintosh Roman
     * "x-MacRomania", // Macintosh ルーマニア "x-MacSymbol", // Macintosh シンボル
     * "x-MacThai", // Macintosh タイ "x-MacTurkish", // Macintosh トルコ語
     * "x-MacUkraine", // Macintosh ウクライナ "x-MS950-HKSCS", // Windows 繁体字中国語
     * (香港の拡張付き) "x-mswin-936", // Windows 簡体字中国語 "x-PCK", // Solaris 版の
     * Shift_JIS "x-windows-50220", // Windows Codepage 50220 (7 ビット実装)
     * "x-windows-50221", // Windows Codepage 50221 (7 ビット実装) "x-windows-874",
     * // Windows タイ語 "x-windows-949", // Windows 韓国語 "x-windows-950", //
     * Windows 繁体字中国語 "x-windows-iso2022jp" // 拡張 ISO-2022-JP (MS932 ベース) };
     */
}

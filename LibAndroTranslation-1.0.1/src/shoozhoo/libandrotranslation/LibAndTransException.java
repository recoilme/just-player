package shoozhoo.libandrotranslation;

public class LibAndTransException extends Exception {

	/**
	 *
	 */
	private static final long serialVersionUID = -4279277193105213343L;

	public LibAndTransException() {
		super();
	}

	public LibAndTransException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public LibAndTransException(String detailMessage) {
		super(detailMessage);
	}

	public LibAndTransException(Throwable throwable) {
		super(throwable);
	}

}
